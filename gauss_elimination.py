#!/usr/bin/env python3 #shebang

import numpy as np
import time


def transpose(mx: []) -> []:  # jednak moj modul na macierze sie przydał xdd
    """Transposes the matrix"""
    temp_matrix = mx
    matrix = []
    columns = len(temp_matrix)
    rows = len(temp_matrix[0])

    for row in range(0, rows):
        matrix.append([])
        for column in range(0, columns):
            value = temp_matrix[column][row]
            matrix[row].append(value)
    return matrix


class GaussElimination:
    """this class handles the performance
    of gauss elimination on a given matrix and a vector"""

    def __init__(self, matrix: np.ndarray, vector: np.ndarray):
        self.arcs = []
        self.matrix = self.__merge(matrix, vector)
        self.dimension_n: int = vector.size
        self.num_vertex = 0
        self.tacts = 0
        self.socket = []
        self.edges = []
        self.socket_Fi = []
        self.Ft = []
        self.matrix_F = []
        self.EP_Fi = []
        self.matrix_D = np.array([[0, 0, 0, 0],
                                  [0, 0, 0, 0],
                                  [0, 0, 0, 0]])

        self.matrix_Ft = np.array([[1, 1, 1]])

        self.matrix_Fs = np.array([[0, 0, 1],
                                   [0, 1, 0],
                                   [0, 1, -1],
                                   [1, 0, 0],
                                   [1, 0, -1],
                                   [0, 1, 1],
                                   [1, 0, 1],
                                   [1, -1, 0],
                                   [1, -1, 1],
                                   [1, -1, -1]])
        self.__calc_ep()

    def __sizeof__(self) -> int:
        """this method returns the size of matrix
        :return: N size of matrix"""
        return self.dimension_n

    def __repr__(self) -> np.ndarray:
        """this method returns merged matrix as the representation
        :return: Matrix"""
        return self.matrix

    def get_time(self) -> float:
        """this method returns the operation time
        :return: operation time"""
        return self.op_time

    @staticmethod
    def __merge(matrix: np.ndarray, vector: np.ndarray) -> np.ndarray:
        """this method merges the matrix with the vector
        into one matrix
        :return: Matrix merged with vector"""
        return np.hstack((matrix, vector))

    def __do_the_thing(self) -> None:
        """this method basically does the whole thing
        of gauss elimination &stuff. Idk dude it just works"""
        for step in range(self.dimension_n - 1):
            if self.matrix[step, step] == 0.0:
                self.matrix[row, column] = self.matrix[row, column]

            for row in range(step + 1, self.dimension_n):
                factor = self.matrix[row, step] / self.matrix[step, step]

                for column in range(self.dimension_n + 1):
                    self.matrix[row, column] -= self.matrix[step, column] * factor

            print(F"Matrix after step: {step + 1}")
            print(self.matrix)

    def __operation_time(self) -> None:
        clock_start = time.perf_counter()
        self.__do_the_thing()
        clock_end = time.perf_counter()
        self.op_time = round((clock_end - clock_start) * 1000, 4)

    def __nested_loops(self) -> None:
        """This method returns nested loops of socket."""
        self.__operation_time()
        print(f"\nCzas wykonywania rozkladu: {self.get_time()}")
        print("\n=================================\n")
        print("Lista wierzchołków")
        print("Lp.\t W1\t W2\t Im\t Ia2\t Ia1")
        for W1 in range(self.dimension_n - 1):
            for W2 in range(W1 + 1, self.dimension_n):
                self.socket.append([self.num_vertex, W1, W2, W1, (W2, W1), (W1, W1), (W2, W1), 1])
                print(self.socket[self.num_vertex])
                self.num_vertex += 1

            for W3 in range(W1 + 1, self.dimension_n):
                for W2 in range(W1 + 1, self.dimension_n):
                    self.socket.append([self.num_vertex, W1, W2, W3, (W2, W1), (W1, W3), (W2, W3), 2])
                    print(self.socket[self.num_vertex])
                    self.num_vertex += 1

        num_edge = 0
        num_direction = 0
        for col in range(self.dimension_n, self.dimension_n + 3):
            for row in range(len(self.socket)):
                for next_row in range(row + 1, len(self.socket)):
                    if self.socket[row][col] == self.socket[next_row][col]:
                        self.edges.append([num_edge,
                                           (self.socket[row][1], self.socket[row][2], self.socket[row][3]),
                                           (self.socket[next_row][1], self.socket[next_row][2],
                                            self.socket[next_row][3])])
                        self.arcs.append(
                            (self.socket[row][0], self.socket[next_row][0]))
                        num_edge += 1
                        break
            distance_edge = (self.edges[-1][2][0] - self.edges[-1][1][0],
                             self.edges[-1][2][1] - self.edges[-1][1][1],
                             self.edges[-1][2][2] - self.edges[-1][1][2])
            for item in range(3):
                self.matrix_D[item][num_direction] = distance_edge[item]
            num_direction += 1
        for row in range(len(self.socket)):
            for next_row in range(row + 1, len(self.socket)):
                if self.socket[row][6] == self.socket[next_row][5]:
                    if abs(self.socket[next_row][1] - self.socket[row][1]) <= 1:
                        self.edges.append([num_edge,
                                           (self.socket[row][1], self.socket[row][2], self.socket[row][3]),
                                           (self.socket[next_row][1], self.socket[next_row][2],
                                            self.socket[next_row][3])])
                        self.arcs.append(
                            (self.socket[row][0], self.socket[next_row][0]))
                        num_edge += 1
                        break
        distance_edge = (self.edges[-1][2][0] - self.edges[-1][1][0],
                         self.edges[-1][2][1] - self.edges[-1][1][1],
                         self.edges[-1][2][2] - self.edges[-1][1][2])
        for item in range(3):
            self.matrix_D[item][num_direction] = distance_edge[item]
        num_direction += 1

        print("\n=================================\n")
        print(F"\nLista krawedzi:")
        for row in self.edges:
            print(row)

        # print(F"\nList of arcs:")
        # for i, arc in enumerate(self.arcs):
        #     print(i, end=": ")
        #     print(arc)

        print("\n=================================\n")
        print(F"\nMatrix D:")
        for row in self.matrix_D:
            print(row)

    def __calculate_matrix_f(self):
        """ Ograniczenie lokalnosci -> system rownan liniowych """
        self.__nested_loops()

        max_reps_amount = len(self.matrix_Fs)
        number = 1

        for row in range(max_reps_amount):
            for next_row in range(row + 1, max_reps_amount):
                temp_F = np.vstack([self.matrix_Fs[row], self.matrix_Fs[next_row], self.matrix_Ft])
                if np.linalg.det(temp_F) != 0:
                    # print(f"F {number}:\n{temp_F}")
                    number += 1
                    self.matrix_F.append(temp_F)

        # print(*self.matrix_F, sep="\n")

    def __list_ep(self):
        self.__calculate_matrix_f()

        print("\n=================================\n")
        print("Lista elementów przetwarzajacych")
        print("Lp.\t i\t j\t k\t", end=" ")
        for F in range(0, len(self.matrix_F)):
            print(f"F{F + 1}\t", end=" ")
        print("Ft")

        i = 0
        first_ft = 0
        last_ft = 0
        for socket in self.socket:
            print(f"[{i + 1}, {socket[1]}, {socket[2]}, {socket[3]}, ", end=" ")
            single_socket_Fi = []
            for F in self.matrix_F:
                tupla1 = np.dot(F[0], [socket[1], socket[2], socket[3]])
                tupla2 = np.dot(F[1], [socket[1], socket[2], socket[3]])
                coordinates = (tupla1, tupla2)
                single_socket_Fi.append(coordinates)
                print(f"{coordinates}", end=" ")
            self.socket_Fi.append(single_socket_Fi)
            ft = np.dot([1, 1, 1], [socket[1], socket[2], socket[3]])
            self.Ft.append(ft)
            i += 1
            if i == 1:
                first_ft = ft
            elif i == len(self.socket):
                last_ft = ft
            print(ft)

        self.tacts = last_ft - first_ft + 1
        print(f"Takty = {self.tacts}")

    def __calc_channels(self):
        self.__list_ep()

        i = 1
        for F in self.matrix_F:
            channels = 0
            print(f"Polaczenia miedzy EP dla F{i}: ")
            for column in range(0, self.matrix_D.shape[1]):
                tupla = np.dot([F[0], F[1]], self.matrix_D[:, column])
                comparison = tupla == np.array([0, 0])
                if not comparison.all():
                    print(tupla)
                    channels += 1
            print(f"Liczba kanałów dla F{i} = {channels}")
            i += 1

    def __calc_ep(self):
        self.__calc_channels()

        print("\n=================================\n")
        Fi_socket = transpose(self.socket_Fi)
        best_load = 9
        worst_load = 0
        best_matrix_F = 0
        worst_matrix_F = 0
        for i, F in enumerate(Fi_socket):
            ep = set(F)  # unikalne wartosci w kazdym F -> ilosc ep
            self.EP_Fi.append(len(ep))
            print(f"Ilosc EP dla F{i + 1}: {len(ep)}", end="\t")
            load = len(self.socket) / (len(ep) * self.tacts)
            print(f"Obciążenie dla F{i + 1}: {load:.3f}")

            if best_load >= load:
                best_load = load
                best_matrix_F = i + 1

            if worst_load <= load:
                worst_load = load
                worst_matrix_F = i + 1

            if i == len(Fi_socket)-1:
                print("\n=================================\n")
                print(f"Najlepsze F{best_matrix_F} = {best_load:.3f}")
                print(f"Najgorsze F{worst_matrix_F} = {worst_load:.3f}")


matrix_a = np.array([[2., 4., 2., 0.],
                     [1., 0., -1., 1.],
                     [0., 1., 3., -1.],
                     [2., 1., 2., 1.]])

vector_a = np.array([[4.],
                     [2.],
                     [0.],
                     [6.]])

GaussElimination(matrix_a, vector_a)
